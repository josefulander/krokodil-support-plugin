<?php
/**
* Plugin Name: krokodil service & underhåll
* Plugin URI: https://krokodil.io/
* Description: Service & underhållslogg.
* Version: 1.0
* Author: Josef Ulander
* Author URI: https://krokodil.io/
**/

/***************************************
* Creating a function to create our CPT
***************************************/
 
function krokodil_maintenance_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => 'Logg',
        'singular_name'       => 'Logg',
        'menu_name'           => 'Underhållslogg',
        'all_items'           => 'Alla loggar',
        'view_item'           => 'View Movie',
        'add_new_item'        => 'Ny logg',
        'add_new'             => 'Ny logg',
        'edit_item'           => 'Redigera logg',
        'update_item'         => 'Uppdatera loggar',
        'search_items'        => 'Sök bland loggar',
        'not_found'           => 'Inga träffar',
        'not_found_in_trash'  => 'Inga träffar i papperskorg',
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => 'Underhåll',
        'description'         => 'Underhållsloggar',
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor','author', 'thumbnail','revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        //'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        //'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => false,
        'menu_position'       => 999,
		'menu_icon'           => 'dashicons-admin-tools',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => false,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'maintenance', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'krokodil_maintenance_post_type', 0 );


/***************************************
* Hide menuitem from other users
***************************************/

function hide_menu(){
	global $current_user;
		get_currentuserinfo();
		$user_name = $current_user->user_login;
      	
		if ( $user_name != 'josefulander') {
			remove_menu_page( 'edit.php?post_type=maintenance' );
		} 
}

add_action('admin_head', 'hide_menu');

/***************************************
* Add dashboard widget
***************************************/

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
  
function my_custom_dashboard_widgets() {
global $wp_meta_boxes;
 
wp_add_dashboard_widget('custom_help_widget', 'Krokodil Underhåll & Support', 'custom_dashboard_help');
}
 
function custom_dashboard_help() {
	echo '<div class="krokodil-top-section"><p>Tack för att du använder dig av krokodil webbproduktion.<br>Behöver du hjälp? Det är bara att du hör av dig!</p>';
	echo '<div class="krokodil-contact"><span><span class="dashicons dashicons-email-alt"></span><a class="krokodil-mail" href="mailto:josef@krokodil.io">josef@krokodil.io</a></span><span><span class="dashicons dashicons-smartphone"></span><a class="krokodil-phone" href="tel:0703837455">070-383 74 55</a></span><span><span class="dashicons dashicons-admin-site"></span><a class="krokodil-web" href="http://krokodil.io">krokodil.io</a></span></div></div>'
	?>
	<h2 class="krokodil-log-title sub">Tilläggspaket</h2>
	<div class="krokodil-bottom-section">
		<span class="dashicons dashicons-warning"></span>
	</div>
	<h2 class="krokodil-log-title sub">Underhållslogg</h2>
	<div class="krokodil-bottom-section">
		<ul>
		 <?php
			  global $post;
			  $args = array( 'numberposts' => 4, 'post_type' => 'maintenance' );

			  $myposts = get_posts( $args );
			foreach( $myposts as $post ) :  setup_postdata($post); ?>
			<li> <span class="krokodil-date"><? the_date('Y-n-d'); ?></span> | <span class="krokodil-title"><?php the_title(); ?></span><?php the_content(); ?></li>
		  <?php endforeach; ?>
		</ul>
	</div>
<?php
}

/***************************************
* Style dashboard CSS
***************************************/

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #custom_help_widget.postbox {
      /*background: #212121;*/
	  /*color: #f5f5f4;*/
	  /*border: 1px solid #212121;*/
    }
	#custom_help_widget.postbox h2{
	  /*color: #f5f5f5;*/
    }
	#custom_help_widget.postbox .hndle, #custom_help_widget .stuffbox .hndle {
    	/*border-bottom: 1px solid rgb(64,64,64);*/
	}
	#custom_help_widget h2.hndle {
		background-image: url("https://krokodil.io/wp-content/uploads/2019/10/Logo-color-tube.svg");
		background-repeat: no-repeat;
		background-size: 64px auto;
		background-position: calc(100% - 36px) 50%;
	}
	.krokodil-contact {
		display: flex;
		justify-content: space-between;
		margin: 2em 0em 1em 0em;
	}
	#custom_help_widget a {
		/*color: #f5f5f5;*/
	}
	#custom_help_widget a:hover {
		color: #7137C8;
	}
	#custom_help_widget .dashicons {
		margin-right: .3em;
		color: #7137C8;
	}
	#custom_help_widget .inside {
		padding: 0px;
	}
	.krokodil-top-section {
		padding: 0px 12px;
	}
	.krokodil-bottom-section {
		padding: 6px 12px;
		/*background-color: rgb(64,64,64);*/
	}
	#custom_help_widget.postbox h2.krokodil-log-title {
		font-size: 14px;
		/*background-color: rgb(64,64,64);*/
		margin: 0;
    	padding: 8px 12px;
    	line-height: 18px;
		/*border-bottom: 1px solid rgb(94,94,94);*/
	}
	#custom_help_widget.postbox ul li {
		margin-top: .4em;
		/*color: rgb(120,120,120);*/
	}
	.krokodil-title ,
	.krokodil-date {
		/*color: #2196f3;*/
	}
	#custom_help_widget.postbox ul li p {
		margin-top: .4em;
		/*color: #f5f5f5;*/
	}
	
  </style>';
}